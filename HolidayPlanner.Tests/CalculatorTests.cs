using System;
using NUnit.Framework;
using HolidayPlanner.HolidayCalculator;

namespace HolidayPlanner.Tests
{
    public class Tests
    {
        private DayCalculator _dayCalculator;
        
        [SetUp]
        public void Setup()
        {
            var nationalHolidays = new[]
            {
                DateTime.Parse("1.1.2021"),
                DateTime.Parse("6.1.2021"),
                DateTime.Parse("2.4.2021"),
                DateTime.Parse("5.4.2021"),
                DateTime.Parse("13.5.2021"),
                DateTime.Parse("25.6.2021"),
                DateTime.Parse("6.12.2021"),
                DateTime.Parse("24.12.2021"),
                DateTime.Parse("1.1.2022"),
                DateTime.Parse("6.1.2022"),
                DateTime.Parse("15.4.2022"),
                DateTime.Parse("18.4.2022"),
                DateTime.Parse("1.5.2022"),
                DateTime.Parse("26.5.2022"),
                DateTime.Parse("5.6.2022"),
                DateTime.Parse("24.6.2022"),
                DateTime.Parse("25.6.2022"),
                DateTime.Parse("6.12.2022"),
                DateTime.Parse("24.12.2022"),
                DateTime.Parse("25.12.2022"),
                DateTime.Parse("26.12.2022"),
            };

            _dayCalculator = new DayCalculator(nationalHolidays);
        }

        [Test]
        public void Return4Days()
        {
            // Monday 1.3.2021
            var start = new DateTime(2021, 3, 1);
            // Thursday 4.3.2021
            var end = start.AddDays(3); 

            var result = _dayCalculator.GetNumberOfHolidays(start, end);

            Assert.AreEqual(4, result);
        }

        [Test]
        public void DontCountNationalHolidays()
        {
            // Monday 4.1.2021
            var start = new DateTime(2021, 1, 4); 
            // Thursday 7.1.2021
            var end = start.AddDays(3);

            var result = _dayCalculator.GetNumberOfHolidays(start, end);

            // Wednesday 6.1.2021 is a national holiday
            Assert.AreEqual(3, result);
        }
        
        [Test]
        public void SaturdaysConsumeHolidays()
        {
            // Monday 1.3.2021
            var start = new DateTime(2021, 3, 1);
            // Friday 5.3.2021
            var end = start.AddDays(4); 

            var result = _dayCalculator.GetNumberOfHolidays(start, end);

            Assert.AreEqual(6, result);
        }

        [Test]
        public void SundaysDontConsumeHolidays()
        {
            // Monday 1.3.2021
            var start = new DateTime(2021, 3, 1);
            // Sunday 7.3.2021
            var end = start.AddDays(6); 

            var result = _dayCalculator.GetNumberOfHolidays(start, end);

            Assert.AreEqual(6, result);
        }
        
        [Test]
        public void ThrowsExceptionOnNotFitPeriod()
        {
            // Monday 1.3.2021
            var start = new DateTime(2021, 3, 1);
            // Thursday 1.4.2021
            var end = new DateTime(2021, 4, 1);
            
            Assert.Throws<HolidaySpanOutsidePeriodException>(() => { _dayCalculator.GetNumberOfHolidays(start, end);});
        }

        [Test]
        public void ThrowsExceptionOnExceedMaximumTimeSpan()
        {
            // Monday 1.2.2021
            var start = new DateTime(2021, 2, 1);
            // Wednesday 31.3.2021
            var end = new DateTime(2021, 3, 31);

            Assert.Throws<HolidaySpanTooLongException>(() => { _dayCalculator.GetNumberOfHolidays(start, end);});
        }
    }
}