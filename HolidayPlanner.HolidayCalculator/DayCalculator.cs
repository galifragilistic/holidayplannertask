﻿using System;
using System.Linq;

namespace HolidayPlanner.HolidayCalculator
{
    public class DayCalculator
    {
        private readonly DateTime[] _nationalHolidays;

        public DayCalculator(DateTime[] nationalHolidays)
        {
            _nationalHolidays = nationalHolidays;
        }
        
        public int GetNumberOfHolidays(DateTime start, DateTime end)
        {
            if (start > end)
            {
                throw new Exception("You got your dates mixed up buddy");
            }
            
            if (!IsInsideMaximumLimit(start, end))
            {
                throw new HolidaySpanTooLongException();
            }
            
            if (!IsInsideOnePeriod(start, end))
            {
                throw new HolidaySpanOutsidePeriodException();
            }
            
            var date = start.Date;
            var totalHolidays = 0;
            
            while (date != end.Date.AddDays(1))
            {
                if (date.DayOfWeek != DayOfWeek.Sunday && !IsNationalHoliday(date))
                {
                    totalHolidays++;
                }

                date = date.AddDays(1);

                // if the last day is Friday and the time span is longer than 5 days, count Saturday too (unless its a national holiday)
                if (date == end.Date 
                    && date.DayOfWeek == DayOfWeek.Friday
                    && !IsNationalHoliday(date.AddDays(1))
                    && (end.AddDays(1)-start).Days >= 5)
                {
                    totalHolidays++;
                }
            }
            
            return totalHolidays;
        }

        private bool IsInsideOnePeriod(DateTime start, DateTime end)
        {
            // period starts in 1.4 and ends in 31.3
            var year = start.Month >= 4 ? start.Year : start.Year - 1;
            
            var periodStart = new DateTime(year, 4, 1);
            var periodEnd = new DateTime(year + 1, 3, 31);

            return start.Date >= periodStart 
                   && end.Date <= periodEnd;
        }

        private bool IsInsideMaximumLimit(DateTime start, DateTime end)
        {
            return (end.AddDays(1) - start).Days <= 50;
        }

        private bool IsNationalHoliday(DateTime date)
        {
            return _nationalHolidays.Contains(date.Date);
        }
    }
}